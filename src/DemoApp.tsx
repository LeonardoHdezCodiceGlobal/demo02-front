import { AppTheme } from './theme'
import { RouterProvider } from 'react-router-dom'
import router from './router'
import { Navbar } from './components/organismos'

function App () {
  return (
    <>
      <AppTheme>
        <Navbar />
        <RouterProvider router={router} />
      </AppTheme>
    </>
  )
}

export default App
