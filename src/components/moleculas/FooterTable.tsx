import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos'
import { useEffect, useState } from 'react'

interface FooterTableProps {
  count: number
  page: number
  rowsPerPage: number
  onPageChange: (newPage: number) => void
  onRowsPerPageChange: (newRowsPerPage: number) => void
}

export const FooterTable = ({ count, page, rowsPerPage, onRowsPerPageChange, onPageChange, }: FooterTableProps) => {
  const [displayValue, setDisplayValue] = useState(rowsPerPage)

  useEffect(() => {
    setDisplayValue(Math.min(Number(rowsPerPage), count))
  }, [rowsPerPage])

  const handlePageChange = (direction: 'prev' | 'next') => {
    const newPage = direction === 'prev' ? page - 1 : page + 1
    onPageChange(newPage)
  }

  const handleRowsPerPageChange = (event: React.ChangeEvent<any>) => {
    const newRowsPerPage = event.target.value
    onRowsPerPageChange(newRowsPerPage)
    console.log('Cambio de filas por página: ', newRowsPerPage)
  }
  return (
    <div className='bg-[#EBEFF2] p-5 rounded-3xl mt-5 flex justify-between items-center'>
      <div>
        <p>Total de registros: {count}</p>
      </div>
      <div className='flex gap-3 items-center'>
        <p>Filas por pagina:</p>
        <div>
          <select value={rowsPerPage} className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" onChange={handleRowsPerPageChange}>
            <option value="10">10</option>
            <option value="15">15</option>
            <option value="20">20</option>
          </select>
        </div>
        <p> 1 - {displayValue} de {count}</p>
        <div>
          <button
            className='bg-white rounded-full p-2 disabled:bg-[#BCC4D0] disabled:cursor-not-allowed'
            onClick={() => handlePageChange('prev')}
            disabled={page === 1}>
            <ArrowBackIosIcon fontSize='small' />
          </button>
          <button
            className="rotate-180 bg-white rounded-full p-2 disabled:cursor-not-allowed disabled:bg-[#BCC4D0]"
            onClick={() => handlePageChange('next')}
            disabled={displayValue >= count}>
            <ArrowBackIosIcon fontSize='small' />
          </button>
        </div>
      </div>
    </div>
  )
}
