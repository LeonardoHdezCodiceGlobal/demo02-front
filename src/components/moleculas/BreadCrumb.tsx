import { Link, Typography } from '@mui/material'
import Breadcrumbs from '@mui/material/Breadcrumbs'

export const BreadCrumb = ({ page }: { page: string }) => {
  const handleClick = (event: React.MouseEvent) => {
    event.preventDefault()
    console.info('You clicked a breadcrumb.')
  }

  return (
    <div role="presentation" onClick={handleClick}>
      <Breadcrumbs aria-label="breadcrumb">
        <Link underline="hover" color="black" href="/">
          Formas Valoradas
        </Link>
        <Typography color="text.primary">{page}</Typography>
      </Breadcrumbs>
    </div>
  )
}
