export const Navbar = () => {
  return (
    <ul className="flex flex-row flex-wrap px-6 bg-[#2C5384] select-none">
      <li className="flex flex-initial box-border hover:bg-[#6187b8]">
        <p className="text-white w-full h-full p-4 font-semibold hover:cursor-pointer">Ingresos</p>
      </li>
      <li className="flex flex-initial box-border hover:bg-[#6187b8]">
        <p className="text-white w-full h-full p-4 font-semibold hover:cursor-pointer">Asignaciones</p>
      </li>
    </ul>
  )
}
