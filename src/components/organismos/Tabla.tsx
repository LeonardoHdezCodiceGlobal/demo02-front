import { DataGrid, GridColDef } from '@mui/x-data-grid'
import { FooterTable } from '../moleculas/FooterTable'
import { useEffect, useState } from 'react'

export const Tabla = ({ data, columns }: { data: any, columns: GridColDef[] }) => {
  const [currentData, setCurrentData] = useState([])
  const [page, setPage] = useState(1)
  const [paginationModel, setPaginationModel] = useState({ pageSize: 25, page: 0 })
  const [rowsPerPage, setRowsPerPage] = useState(10)


  const handlePageChange = (newPage: number) => {
    setPage(newPage)
  }

  const handleRowsPerPageChange = (newRowsPerPage: number) => {
    setRowsPerPage(newRowsPerPage)
  }

  useEffect(() => {
    const startIndex = (page - 1) * rowsPerPage
    const endIndex = startIndex + rowsPerPage

    setCurrentData(data.slice(startIndex, endIndex))
  }, [rowsPerPage, page])



  return (
    <div>
      <DataGrid
        sx={{
          borderRadius: '200px',
          '.MuiDataGrid-columnSeparator': { display: 'none', },
          '&.MuiDataGrid-root': { border: 'none', borderRadius: 5 },
          '.super-app-theme--header': { backgroundColor: '#2C5384', color: 'white', ":nth-of-type(1)": { borderTopLeftRadius: '16px' }, ":nth-last-of-type(1)": { borderTopRightRadius: '16px' } },
        }}
        checkboxSelection={false}
        columnHeaderHeight={40}
        columns={columns}
        disableColumnMenu
        pageSizeOptions={[10, 15, 20]}
        paginationModel={paginationModel}
        onPaginationModelChange={setPaginationModel}
        rowHeight={30}
        rows={currentData}
        slots={{
          footer: () =>
            <FooterTable
              count={data.length}
              page={page}
              rowsPerPage={rowsPerPage}
              onPageChange={handlePageChange}
              onRowsPerPageChange={handleRowsPerPageChange} />
        }}
      />
    </div>
  )
}
