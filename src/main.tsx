import React from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'
import { RouterProvider } from 'react-router-dom'
import router from './router.tsx'
import { Navbar } from './components/organismos/Navbar.tsx'

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <Navbar />
    <div className='px-5 pb-5'>
      <RouterProvider router={router} />
    </div>
  </React.StrictMode>
)
