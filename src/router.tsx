import { createBrowserRouter } from 'react-router-dom'
import { PublicRoute } from './router/PublicRoute'
import { FormasValoradasPage } from './views/FormasValoradas/FormasValoradasPage'
import { IngresosPage } from './views/Ingresos/IngresosPage'
import { AsignacionesPage } from './views/Asignaciones/AsignacionesPage'

export default createBrowserRouter([
  {
    path: '/',
    element: <PublicRoute />,
    children: [
      { index: true, element: <FormasValoradasPage /> },
      { path: 'ingresos', element: <IngresosPage /> },
      { path: 'asignaciones', element: <AsignacionesPage /> }
    ]
  }
])
