import { CssBaseline, ThemeProvider } from '@mui/material'
import { ReactNode } from 'react'
import { theme } from '.'

export const AppTheme = ({ children }: { children: ReactNode }) => {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      {children}
    </ThemeProvider>
  )
}
