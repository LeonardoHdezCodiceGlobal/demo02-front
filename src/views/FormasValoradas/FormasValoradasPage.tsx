import { GridColDef } from '@mui/x-data-grid'
import { BreadCrumb } from '../../components/moleculas/BreadCrumb'
import { Tabla } from '../../components/organismos/Tabla'

export const FormasValoradasPage = () => {
  const columnas: GridColDef[] = [
    {
      field: 'clave',
      headerName: 'Clave',
      headerClassName: 'super-app-theme--header',
      flex: 1
    },
    {
      field: 'department',
      headerName: 'Departamento',
      headerClassName: 'super-app-theme--header',
      flex: 1
    },
    {
      field: 'date',
      headerName: 'Fecha',
      headerClassName: 'super-app-theme--header',
      flex: 1
    },
    {
      field: 'status',
      headerName: 'Estatus',
      headerClassName: 'super-app-theme--header',
      flex: 1
    },
  ]

  const data = [
    {
      id: 1,
      clave: '0008',
      department: '014 Querétaro',
      date: '25/05/2022',
      status: 'Pendiente',
    },
    {
      id: 2,
      clave: '0540',
      department: '014 Querétaro',
      date: '18/09/2023',
      status: 'Pendiente',
    },
    {
      id: 3,
      clave: '0018',
      department: '015 Querétaro',
      date: '25/05/2022',
      status: 'Pendiente',
    },
    {
      id: 4,
      clave: '0530',
      department: '014 Querétaro',
      date: '18/09/2023',
      status: 'Activo',
    },
    {
      id: 5,
      clave: '0009',
      department: '014 Querétaro',
      date: '25/05/2022',
      status: 'Pendiente',
    },
    {
      id: 6,
      clave: '0541',
      department: '014 Querétaro',
      date: '18/09/2023',
      status: 'Pendiente',
    },
    {
      id: 7,
      clave: '0019',
      department: '015 Querétaro',
      date: '25/05/2022',
      status: 'Pendiente',
    },
    {
      id: 8,
      clave: '0532',
      department: '014 Querétaro',
      date: '18/09/2023',
      status: 'Activo',
    },
    {
      id: 9,
      clave: '0023',
      department: '014 Querétaro',
      date: '25/05/2022',
      status: 'Pendiente',
    },
    {
      id: 10,
      clave: '0585',
      department: '014 Querétaro',
      date: '18/09/2023',
      status: 'Pendiente',
    },
    {
      id: 22,
      clave: '0085',
      department: '015 Querétaro',
      date: '25/05/2022',
      status: 'Pendiente',
    },
    {
      id: 11,
      clave: '053',
      department: '014 Querétaro',
      date: '18/10/2023',
      status: 'Activo',
    },
  ]

  return (
    <div>
      <BreadCrumb page='Catálogo de Años de Producción' />
      <Tabla columns={columnas} data={data} />
    </div>
  )
}
